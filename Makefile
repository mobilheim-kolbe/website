all: generate build push

generate:
	yarn generate

build:
	docker build -t registry.gitlab.com/mobilheim-kolbe/website .

login:
	docker login registry.gitlab.com

push:
	docker push registry.gitlab.com/mobilheim-kolbe/website