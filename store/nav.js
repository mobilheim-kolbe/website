export const state = () => ({
  items: [
    {
      icon: 'mdi-apps',
      title: 'Home',
      to: '/'
    },
    {
      icon: 'mdi-home-outline',
      title: 'Hauser',
      to: '/mobilheime/hauser',
    },
    {
      icon: 'mdi-home-outline',
      title: 'Letniskowo',
      to: '/mobilheime/letniskowo',
    },
    {
      icon: 'mdi-home-analytics',
      title: 'Gebraucht',
      to: '/gebraucht'
    },
    {
      icon: 'mdi-card-account-phone-outline',
      title: 'Kontakt',
      to: '/kontakt'
    },
    {
      icon: 'mdi-shield-sun-outline',
      title: 'Datenschutzerklärung',
      to: '/datenschutz'
    },
    {
      icon: 'mdi-book-outline',
      title: 'Impressum',
      to: '/impressum'
    },
  ],
})