# Website-Source: www.mobilheim-kolbe.de

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Developers

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev
```

## Build & Deploy

```bash
make all  # wraps `make generate build push`
```

The website can then be deployed via Docker.

### Example `docker-compose.yml`

```yaml
  mobilheime:
    image: registry.gitlab.com/mobilheim-kolbe/website:latest
    restart: unless-stopped
    ports:
      - "80"
```