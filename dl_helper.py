#!/usr/bin/env python

import requests
import shutil
import sys
import threading

from bs4 import BeautifulSoup
from tldextract import extract


fetch_url = sys.argv[1]
tsd, td, tsu = extract(fetch_url)
domain = td + '.' + tsu

print(f"Scanning: {fetch_url}")
r = requests.get(fetch_url)
html = r.text
soup = BeautifulSoup(html, 'lxml')
links = soup.findAll('img')


def download(url: str, path: str) -> None:
    req = requests.get(url, stream=True)
    with open(path, 'wb') as out_file:
        shutil.copyfileobj(req.raw, out_file)


print("Scanning page...")
i = 1
threads = []
for link in links:
    path = link.get('src')
    url = f'https://{domain}{path}'
    if sys.argv[2] in path:
        print(f"Found: {url}")
        t = threading.Thread(
            target=download,
            args=(url, f'{i:02d}.jpg'),
        )
        threads.append(t)
        t.start()
        i += 1
    else:
        print(f'| {url}')


print("Fetching everything...")
for t in threads:
    t.join()

# print("Fetching groundview...")
# link = soup.find_all('div', {'class': 'product-main-image'})[0]
# path = link.get('style').split("'")[1].split("'")[0]
# url = f'https://{domain}{path}'
# download(url, "01.jpg")

print("Done.")
